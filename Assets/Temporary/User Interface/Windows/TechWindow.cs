﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TechWindow : MonoBehaviour
{
    [SerializeField] public int index;

    [SerializeField] public TextMeshProUGUI title;
    [SerializeField] public TextMeshProUGUI cost;
    [SerializeField] public TextMeshProUGUI effect;
    [SerializeField] public Image icon;

    private ChainProcessor chain;

    public void SetChain(ChainProcessor c)
    {
        if (chain != null)
            chain.onChange -= Refresh;
        chain = c;
        chain.onChange += Refresh;
    }

    public void Refresh()
    {
        /*
        Upgrade u = chain.GetUpgrade(index);
        if (u == null)
        {
            gameObject.SetActive(false);
            return;
        }
        else gameObject.SetActive(true);

        title.text = Localization.GetString(u.id);
        cost.text = Localization.GetString("cost") + " " + u.baseCost.ToString();
        effect.text = Localization.GetString(u.id + "_d");
        icon.sprite = Sprites.GetSprite(u.id);
        */
    }

    public void ClickBuyUpgrade()
    {
        //chain.BuyUpgrade(index);
    }
}
