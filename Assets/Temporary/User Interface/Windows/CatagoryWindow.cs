﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CatagoryWindow : MonoBehaviour
{
    [SerializeField] public int index;

    [SerializeField] public TextMeshProUGUI title;
    [SerializeField] public TextMeshProUGUI cost;
    [SerializeField] public TextMeshProUGUI workers;
    [SerializeField] public TextMeshProUGUI production;
    [SerializeField] public TextMeshProUGUI percentage;
    [SerializeField] public Image icon;

    private ChainProcessor chain;

    public void SetChain(ChainProcessor c)
    {
        if(chain != null)
            chain.onChange -= Refresh;
        chain = c;
        chain.onChange += Refresh;
    }

    public void Refresh()
    {
        //title.text = chain.GetBuildingName(index);
        //cost.text = Localization.GetString("cost") + " " + chain.GetBuildingCost(index).ToString();
        //workers.text = chain.GetBuildingAmount(index).ToString() + " " + Localization.GetString("workers");
        //production.text = chain.GetProduction(index).ToString() + " " + Localization.GetString("sec");
        //percentage.text = chain.GetProductionPercentage(index, 1).ToString() + "%";
        //icon.sprite = chain.GetBuildingSprite(index);
    }

    public void ClickBuyBuilding()
    {
        //chain.BuyBuilding(index);
    }
}