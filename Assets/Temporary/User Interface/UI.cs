﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI : MonoBehaviour
{
    private List<CatagoryWindow> catagories = new List<CatagoryWindow>();
    private List<TechWindow> techs = new List<TechWindow>();
    private Chain currentChain;
    private int currentChainIndex;

    private Vector2 fingerDown;
    private Vector2 fingerUp;

    private void Start()
    {
        foreach (CatagoryWindow c in GetComponentsInChildren<CatagoryWindow>())
            catagories.Add(c);
        foreach(TechWindow t in GetComponentsInChildren<TechWindow>())
            techs.Add(t);
        currentChainIndex = 0;
        SetNewChain(0);
    }

    private void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            
            if (touch.phase == TouchPhase.Began)
            {
                fingerUp = touch.position;
                fingerDown = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                fingerUp = touch.position;
                CheckSwipeDirection();
            }
        }

        // TODO: Delete this, PC use only.
        if (Input.GetKeyDown("1"))
            SetNewChain(currentChainIndex - 1);
        else if (Input.GetKeyDown("2"))
            SetNewChain(currentChainIndex + 1);
    }

    private void CheckSwipeDirection()
    {

        if (fingerDown.x < fingerUp.x)
            SetNewChain(currentChainIndex - 1);
        else if (fingerDown.x > fingerUp.x)
            SetNewChain(currentChainIndex + 1);
    }

    public void SetNewChain(int index)
    {
        /*
        Chain chain = Game.GetChain(index);
        if (chain != null)
        {
            currentChainIndex = index;
            currentChain = chain;
            foreach (CatagoryWindow c in catagories)
            {
                c.SetChain(currentChain);
                c.Refresh();
            }
            foreach (TechWindow t in techs)
            {
                t.SetChain(currentChain);
                t.Refresh();
            }
        }
        */
    }
}
