﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameMath
{
    public static float passiveCostIncrease = 1.0825f;

    public static float GetRoundedPercentage(float value, int decimalPoints)
    {
        int decimalMultiplier = 0;
        for (int i = 0; i < decimalPoints; i++)
        {
            if(decimalMultiplier == 0)
                decimalMultiplier = 10;
            else decimalMultiplier *= 10;
        }
        float v = Mathf.Round(value * decimalMultiplier);
        return v / decimalMultiplier;
    }

    public static string GetIntWithSeperators(float value)
    {
        return string.Format("{0:n0}", value);
    }
}