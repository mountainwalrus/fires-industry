﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum ResourceType { None, Lumber }
public enum UpgradeType { Production, BuildingCost, GoodPrice, Unlock }

public class Game : MonoBehaviour
{
    public GameState state;
    public List<ChainProcessor> productionChains = new List<ChainProcessor>();

    // Initialization
    private void Awake()
    {
        Templates.Load();
        Localization.Load();
        Sprites.Load();

        // TODO: Saving and loading!
        state = new GameState();
        state.globalStats = new GlobalStats();

        Initialize();
    }

    private void Initialize()
    {
        int size = Enum.GetValues(typeof(ResourceType)).Length;
        for (int i = 0; i < state.chains.Count; i++)
        {
            var chain = new ChainProcessor();
            productionChains.Add(chain);
            chain.Initialize(state.chains[i], state.globalStats);
        }
    }

    // Game Loop
    private void Update()
    {
        for (int i = 0; i < productionChains.Count; i++)
            productionChains[i].Process();
        BuyAndSellResources();
    }

    private void BuyAndSellResources()
    {
        List<Resource> storage = state.globalStats.storage;
        for (int i = 0; i < storage.Count; i++)
        {
            if (storage[i].amount < 0)
                state.globalStats.gold += storage[i].amount * storage[i].buyPrice;
            else if (storage[i].amount > 0)
                state.globalStats.gold += storage[i].amount * storage[i].sellPrice;
            storage[i].amount = 0;
        }
    }
}