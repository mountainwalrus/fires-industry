﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Resource
{
    public ResourceType resourceType;
    public float amount;
    public float sellPrice = 1f; // TODO: SET PRICE
    public float buyPrice = 1f; // TODO: SET PRICE

    public Resource(ResourceType type)
    {
        resourceType = type;
    }
}
