﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Building
{
    public string id;

    public int workers = 1;
    public int workerCost;

    public ResourceType output;
    public int baseProduction;
    public float productionModifier = 1f;

    public ResourceType input;
    public int baseConsumption;
    public float consumptionModifier = 1f;

    public Building()
    {

    }

    public Building(BuildingTemplate template)
    {

    }
}
