﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Upgrade", menuName = "Data/Upgrade", order = 1)]
public class UpgradeTemplate : ScriptableObject
{
    [Header("Base Stats")]
    public string id;
    public int cost;
    public bool isGlobalUpgrade;

    [Header("Requirements")]
    public string[] requiredUpgrades;
    public string[] excludedUpgrades;

    [Header("Effect")]
    public UpgradeType type;
    public float amount;
    public string parameter;
}
