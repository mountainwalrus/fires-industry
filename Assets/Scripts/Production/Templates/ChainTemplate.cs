﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Production Chain", menuName = "Data/Chain", order = 0)]
public class ChainTemplate : ScriptableObject
{
    [Header("Base Stats")]
    public string id;
    public BuildingTemplate[] buildings;
    public UpgradeTemplate[] upgrades;
}
