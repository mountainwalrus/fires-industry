﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Building", menuName = "Data/Building", order = 2)]
public class BuildingTemplate : ScriptableObject
{
    [Header("Base Stats")]
    public string id;
    public int cost;

    [Header("Production")]
    public ResourceType input;
    public int baseConsumption;
    public ResourceType output;
    public int baseProduction;
}
