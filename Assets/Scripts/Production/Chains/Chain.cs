﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Chain
{
    public string id;
    public List<string> localUpgrades = new List<string>();
    public List<Building> buildings = new List<Building>();
}