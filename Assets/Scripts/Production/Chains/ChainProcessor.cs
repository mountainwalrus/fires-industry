﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChainProcessor
{
    private protected Chain chain;
    private protected GlobalStats globalStats;
    public Action onChange;

    // Initialization
    public virtual void Initialize(Chain chain, GlobalStats globalStats)
    {
        this.chain = chain;
        this.globalStats = globalStats;
    }

    // Production
    public void Process()
    {
        for (int i = 0; i < chain.buildings.Count; i++)
        {
            if (chain.buildings[i].input != ResourceType.None)
                Consume(chain.buildings[i]);
            Produce(chain.buildings[i]);
        }
    }

    private void Consume(Building building)
    {
        Resource resource = globalStats.storage.Find(e => e.resourceType == building.input);
        if (resource == null)
        {
            resource = new Resource(building.input);
            globalStats.storage.Add(resource);
        }

        float consumption = building.baseConsumption * building.workers * Time.deltaTime;
        consumption *= building.consumptionModifier;
        resource.amount -= consumption;
    }

    private void Produce(Building building)
    {
        Resource resource = globalStats.storage.Find(e => e.resourceType == building.output);
        if (resource == null)
        {
            resource = new Resource(building.output);
            globalStats.storage.Add(resource);
        }

        float production = building.baseProduction * building.workers * Time.deltaTime;
        production *= building.productionModifier;
        resource.amount += production;
    }

    // Buildings
    public void AddBuilding(string idName)
    {
        BuildingTemplate template = Templates.GetBuildingTemplate(idName);
        if (template == null)
            return;
        if (globalStats.gold < GetBuildingPrice(idName))
            return;

        chain.buildings.Add(new Building(template));
        globalStats.gold -= GetBuildingPrice(idName);
        onChange();
    }

    public int GetBuildingPrice(string idName)
    {
        BuildingTemplate template = Templates.GetBuildingTemplate(idName);
        float cost = template.cost * globalStats.globalBuildCost;
        return Mathf.RoundToInt(cost);
    }

    // Upgrades
    public void AddUpgrade(string idName)
    {
        UpgradeTemplate template = Templates.GetUpgradeTemplate(idName);
        if (template == null)
            return;
        if (globalStats.gold < GetBuildingPrice(idName))
            return;

        chain.localUpgrades.Add(template.id);
        globalStats.gold -= GetUpgradePrice(idName);
        onChange();
    }

    public int GetUpgradePrice(string idName)
    {
        UpgradeTemplate template = Templates.GetUpgradeTemplate(idName);
        float cost = template.cost * globalStats.globalUpgradeCost;
        return Mathf.RoundToInt(cost);
    }

    // Workers
    public void AddWorker(string idName)
    {
        // TODO: Adding Workers.
        // TODO: MAKE DECISION: WAGES?
    }

    public int GetWorkerPrice(string idName)
    {
        // TODO: Adding Workers.
        // TODO: MAKE DECISION: WAGES?
        return 0;
    }




    /*
    // Production
    public void AddResources()
    {
        for (int i = 0; i < resource.buildings.Length; i++)
            resource.amountStored += GetProductionPerFrame(i);
    }

    public float GetProduction(int index)
    {
        float baseProd = resource.buildings[index].amount * resource.buildings[index].baseProduction;
        // TODO: Production Modifiers;
        return baseProd;
    }

    public float GetProductionPerFrame(int index)
    {
        float bp = GetProduction(index);
        return bp * Time.deltaTime;
    }

    public float GetProductionPercentage(int index, int decimalPoints)
    {
        float totalProduction = 0;
        for (int i = 0; i < 4; i++)
            totalProduction += GetProduction(i);
        if (totalProduction == 0)
            return 0f;
        float value = (GetProduction(index) / totalProduction) * 100;
        return GameMath.GetRoundedPercentage(value, 2);
    }

    // Sales
    public void SellResources()
    {
        int amount = (int)resource.amountStored;
        float payment = amount * GetMarketPrice();
        globalStats.gold += payment;
        resource.amountStored -= amount;
    }

    public float GetMarketPrice()
    {
        var price = globalStats.prices.Find(e => e.type == type);
        if (price != null)
        {
            float mp = price.currentPrice;
            // TODO: Market Price Modifiers;
            return mp;
        }
        else return 0f;
    }

    // Buildings
    public int GetBuildingAmount(int index)
    {
        return resource.buildings[index].amount;
    }

    public void BuyBuilding(int index)
    {
        if (resource.buildings[index].isUnlocked)
            if (globalStats.gold >= GetBuildingCost(index))
            {
                globalStats.gold -= GetBuildingCost(index);
                IncreaseBuildingCost(index);
                resource.buildings[index].amount++;
            }
        onChange();
    }

    private void IncreaseBuildingCost(int index)
    {
        float newCost = resource.buildings[index].baseCost * GameMath.passiveCostIncrease;
        resource.buildings[index].baseCost = (int)newCost;
    }

    public int GetBuildingCost(int index)
    {
        float baseCost = resource.buildings[index].baseCost;
        // TODO: Build Cost Modifiers;
        return (int)baseCost;
    }

    public string GetBuildingName(int index)
    {
        string name = resource.buildings[index].id;
        return Localization.GetString(name);
    }

    public Sprite GetBuildingSprite(int index)
    {
        string name = resource.buildings[index].id;
        return Sprites.GetSprite(name);
    }

    // Upgrades
    public bool HasUpgrade(string id)
    {
        if (globalStats.purchasedUpgrades.Contains(id))
            return true;
        else return false;
    }

    public bool IsUpgradeAvailable(string id)
    {
        Upgrade u = Array.Find(resource.upgrades, e => e.id == id);
        if (u == null)
            return false;

        bool hasAllPrereqUpgrades = true;
        for (int i = 0; i < u.requiredUpgrades.Length; i++)
            if (!HasUpgrade(u.requiredUpgrades[i]))
            {
                hasAllPrereqUpgrades = false;
                break;
            }

        bool isNotExclusive = true;
        for (int i = 0; i < u.excludedUpgrades.Length; i++)
        {

        }

        if (hasAllPrereqUpgrades && isNotExclusive)
            return true;
        else return false;
    }

    public void BuyUpgrade(int index)
    {
        Upgrade u = GetUpgrade(index);
        bool hasAllPrereqUpgrades = true;
        for (int i = 0; i < u.requiredUpgrades.Length; i++)
            if (!HasUpgrade(u.requiredUpgrades[i]))
                hasAllPrereqUpgrades = false;
        if (hasAllPrereqUpgrades)
            ProcessUpgrade(u);
        onChange();
    }

    public Upgrade GetUpgrade(int index)
    {
        List<Upgrade> techs = new List<Upgrade>();
        for (int i = 0; i < resource.upgrades.Length; i++)
        {
            if (resource.upgrades[i].isBought)
                continue;
            if (IsUpgradeAvailable(resource.upgrades[i].id))
                techs.Add(resource.upgrades[i]);
            if (i >= 4)
                break;
        }

        if (index < techs.Count)
            return techs[index];
        else return null;
    }

    private void ProcessUpgrade(Upgrade upgrade)
    {
        upgrade.isBought = true;
        globalStats.gold -= upgrade.baseCost;

        if (upgrade.isGlobalUpgrade)
            globalStats.purchasedUpgrades.Add(upgrade.id);
        else resource.purchasedUpgrades.Add(upgrade.id);


        if (upgrade.type == UpgradeType.Production)
        {
            for (int i = 0; i < resource.buildings.Length; i++)
                if (resource.buildings[i].id == upgrade.parameter)
                {
                    resource.buildings[i].productionMultiplier += upgrade.amount;
                    return;
                }
        }
        else if (upgrade.type == UpgradeType.BuildingCost)
        {
            for (int i = 0; i < resource.buildings.Length; i++)
                if (resource.buildings[i].id == upgrade.parameter)
                {
                    resource.buildings[i].buildCostMultiplier += upgrade.amount;
                    return;
                }
        }
        else if (upgrade.type == UpgradeType.GoodPrice)
        {
            if (Enum.TryParse(upgrade.parameter, true, out ResourceType type))
                for (int i = 0; i < globalStats.prices.Count; i++)
                    if (globalStats.prices[i].type == type)
                    {
                        globalStats.prices[i].priceModifier += upgrade.amount;
                        return;
                    }
        }
        else if (upgrade.type == UpgradeType.Unlock)
        {
            for (int i = 0; i < resource.buildings.Length; i++)
                if (resource.buildings[i].id == upgrade.parameter)
                {
                    resource.buildings[i].isUnlocked = true;
                    return;
                }
        }
    }
    */
}
