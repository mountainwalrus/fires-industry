﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameState
{
    public GlobalStats globalStats;
    public List<Chain> chains = new List<Chain>();
}

[System.Serializable]
public class GlobalStats
{
    public float gold;
    public List<Resource> storage = new List<Resource>();

    public float globalBuildCost = 1f;
    public float globalUpgradeCost = 1f;
}