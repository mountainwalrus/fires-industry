﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Sprites
{
    public static Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();

    public static void Load()
    {
        foreach (Sprite sprite in Resources.LoadAll<Sprite>("Sprites"))
            sprites.Add(sprite.name, sprite);
    }

    public static Sprite GetSprite(string id)
    {
        if (sprites != null)
            if (sprites.ContainsKey(id))
                return sprites[id];
        return null;
    }
}
