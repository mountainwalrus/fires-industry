﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public static class Localization
{
    private static List<Dictionary<string, string>> locFiles = new List<Dictionary<string, string>>();
    private static string language;

    public static void Load()
    {
        foreach (TextAsset ta in Resources.LoadAll<TextAsset>("Localization"))
            locFiles.Add(JsonConvert.DeserializeObject<Dictionary<string, string>>(ta.text));
        language = "english";
    }

    public static string GetString(string id)
    {
        if (locFiles != null && language != null)
        {
            var localization = locFiles.Find(e => e["LANGUAGE"] == language);
            if (localization != null)
                if (localization.ContainsKey(id))
                    return localization[id];
        }
        return id;
    }
}
