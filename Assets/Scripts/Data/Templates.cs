﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Templates
{
    private static List<ChainTemplate> chains = new List<ChainTemplate>();
    private static List<BuildingTemplate> buildings = new List<BuildingTemplate>();
    private static List<UpgradeTemplate> upgrades = new List<UpgradeTemplate>();

    public static void Load()
    {
        foreach (ChainTemplate template in Resources.LoadAll<ChainTemplate>("Templates/Chains"))
            chains.Add(template);
        foreach (BuildingTemplate template in Resources.LoadAll<BuildingTemplate>("Templates/Buildings"))
            buildings.Add(template);
        foreach (UpgradeTemplate template in Resources.LoadAll<UpgradeTemplate>("Templates/Upgrades"))
            upgrades.Add(template);
    }

    public static ChainTemplate GetChainTemplate(string id)
    {
        return chains.Find(e => e.id == id);
    }

    public static BuildingTemplate GetBuildingTemplate(string id)
    {
        return buildings.Find(e => e.id == id);
    }

    public static UpgradeTemplate GetUpgradeTemplate(string id)
    {
        return upgrades.Find(e => e.id == id);
    }
}
